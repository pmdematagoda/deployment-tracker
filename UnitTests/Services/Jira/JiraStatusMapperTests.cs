using System.Collections.Generic;

using DeploymentTrackerCore.Services.Jira;

using FluentAssertions;

using NUnit.Framework;

namespace UnitTests.Services.Jira {
    [TestFixture]
    public class JiraStatusMapperTests {
        internal struct SingleProject {
            internal const int DefaultInProgressStatus = 23;
            internal const int DefaultCompletedStatus = 245;

            internal const string JiraProjectKey = "DEV";

            internal static IDictionary<string, IDictionary<JiraStatus, IList<int>>> Mapping => new Dictionary<string, IDictionary<JiraStatus, IList<int>>> {
                {
                JiraProjectKey,
                new Dictionary<JiraStatus, IList<int>> { { JiraStatus.COMPLETED, new List<int> { DefaultCompletedStatus } },
                { JiraStatus.IN_PROGRESS, new List<int> { DefaultInProgressStatus } },
                }
                }
            };
        }

        internal struct MultipleProjects {
            internal struct FirstJiraProject {
                internal const string Key = "TEST";
                internal const int DefaultInProgressStatus = 23;
                internal const int DefaultCompletedStatus = 245;
            }

            internal struct SecondJiraProject {
                internal const string Key = "DEV";
                internal const int DefaultInProgressStatus = 46;
                internal const int OtherInProgressStatus = 23;
                internal const int DefaultCompletedStatus = 12;
                internal const int OtherCompletedStatus = 98;
            }

            internal static IDictionary<string, IDictionary<JiraStatus, IList<int>>> Mapping => new Dictionary<string, IDictionary<JiraStatus, IList<int>>> {
                {
                FirstJiraProject.Key, new Dictionary<JiraStatus, IList<int>> { { JiraStatus.COMPLETED, new List<int> { FirstJiraProject.DefaultCompletedStatus } },
                { JiraStatus.IN_PROGRESS, new List<int> { FirstJiraProject.DefaultInProgressStatus } },
                }
                },
                {
                SecondJiraProject.Key,
                new Dictionary<JiraStatus, IList<int>> { { JiraStatus.COMPLETED, new List<int> { SecondJiraProject.DefaultCompletedStatus, SecondJiraProject.OtherCompletedStatus } },
                { JiraStatus.IN_PROGRESS, new List<int> { SecondJiraProject.DefaultInProgressStatus, SecondJiraProject.OtherInProgressStatus } },
                }
                }
            };
        }

        [Test]
        public void ASingleProjectMappingCanBeInstantiated() => new JiraStatusMapper(SingleProject.Mapping);

        [Test]
        public void AMultipleProjectMappingCanBeInstantiated() => new JiraStatusMapper(MultipleProjects.Mapping);

        [Test]
        public void TheCompletedStatusCanBeLookedUpForASingleProjectMapping() => new JiraStatusMapper(SingleProject.Mapping)
            .Map(SingleProject.JiraProjectKey, SingleProject.DefaultCompletedStatus).Should().Be(JiraStatus.COMPLETED);

        [Test]
        public void TheInProgressStatusCanBeLookedUpForASingleProjectMapping() => new JiraStatusMapper(SingleProject.Mapping)
            .Map(SingleProject.JiraProjectKey, SingleProject.DefaultInProgressStatus).Should().Be(JiraStatus.IN_PROGRESS);

        [Test]
        public void AnInvalidJiraProjectKeyReturnsUnknown() => new JiraStatusMapper(SingleProject.Mapping)
            .Map("what even is this?", SingleProject.DefaultInProgressStatus).Should().Be(JiraStatus.UNKNOWN);

        [Test]
        public void AnInvalidStatusReturnsUnknown() => new JiraStatusMapper(SingleProject.Mapping)
            .Map(SingleProject.JiraProjectKey, 213123).Should().Be(JiraStatus.UNKNOWN);

        [Test]
        public void TheCompletedStatusCanBeLookedUpForAMultiProjectMapping() => new JiraStatusMapper(MultipleProjects.Mapping)
            .Map(MultipleProjects.FirstJiraProject.Key, MultipleProjects.FirstJiraProject.DefaultCompletedStatus).Should().Be(JiraStatus.COMPLETED);

        [Test]
        public void TheInProgressStatusCanBeLookedUpForAMultiProjectMapping() => new JiraStatusMapper(MultipleProjects.Mapping)
            .Map(MultipleProjects.SecondJiraProject.Key, MultipleProjects.SecondJiraProject.OtherInProgressStatus).Should().Be(JiraStatus.IN_PROGRESS);
    }
}