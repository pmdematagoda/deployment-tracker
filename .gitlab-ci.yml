# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

include:
  - template: Security/License-Scanning.gitlab-ci.yml

image: mcr.microsoft.com/dotnet/sdk:7.0

stages:
- build
- test
- publish-artifacts
- publish-docker
- publish-docker-release

server-build:
  stage: build
  needs: []
  variables:
    IdentitySource__Type: "MockStore"
    ConnectionStrings__Application: "Data Source=deployment.db"
  before_script:
  - dotnet tool restore
  script:
  - dotnet build
  - dotnet dotnet-ef database update --project DeploymentTrackerCore
  - mv DeploymentTrackerCore/deployment.db .
  - dotnet dotnet-ef migrations script -o database-upgrade.sql --project DeploymentTrackerCore
  artifacts:
    name: server-database
    paths:
    - deployment.db
    - database-upgrade.sql
    expire_in: 1 week

client-build:
  stage: build
  needs: []
  image: node
  before_script:
  - cd DeploymentTrackerCore/Frontend
  - npm ci
  script:
  - npm run build-production
  artifacts:
    name: frontend-resources
    paths:
    - DeploymentTrackerCore/wwwroot
    expire_in: 1 week

client-test:
  stage: test
  needs: []
  image: node
  before_script:
  - cd DeploymentTrackerCore/Frontend
  - npm ci
  script:
  - npm test
  artifacts:
    reports:
      junit: DeploymentTrackerCore/Frontend/__tests__/test-report.xml

server-unit-test:
  stage: test
  needs: []
  before_script:
  - cd UnitTests
  script:
  - dotnet test --logger "junit;LogFilePath=junit-test-report.xml"
  artifacts:
    reports:
      junit: UnitTests/junit-test-report.xml

server-integration-test:
  stage: test
  needs: []
  variables:
    ConnectionStrings__Application: "Data Source=deployment.db"
    AllowManualDeploymentsToBeAdded: "true"
    Jira__Enabled: "false"
    ExternalToken: "foobar"
    IdentitySource__Type: "MockStore"
    IdentitySource__Users__0__Username: "jenkins-user"
    IdentitySource__Users__0__Name: "Jenkins"
    IdentitySource__Users__0__Password: "test-password"
  before_script:
  - cd IntegrationTests
  script:
  - dotnet test --logger "junit;LogFilePath=junit-test-report.xml"
  artifacts:
    reports:
      junit: IntegrationTests/junit-test-report.xml

linux-publish:
  stage: publish-artifacts
  needs:
  - client-build
  - server-build
  dependencies:
  - client-build
  - server-build
  script:
  - dotnet publish DeploymentTrackerCore -c Release -r linux-x64 -o deployment-tracker/
  - cp deployment.db deployment-tracker/
  - cp database-upgrade.sql deployment-tracker/
  - mv deployment-tracker/DeploymentTrackerCore deployment-tracker/deployment-tracker
  artifacts:
    name: deployment-tracker-linux-x64
    paths:
    - deployment-tracker/
    expire_in: 1 week

docker-artifacts-publish:
  stage: publish-artifacts
  needs:
  - client-build
  - server-build
  dependencies:
  - client-build
  - server-build
  script:
  - dotnet publish DeploymentTrackerCore -c Release -o docker-app/built-app
  artifacts:
    name: docker-publish
    paths:
    - docker-app/built-app
    expire_in: 1 day

docker-publish:
  stage: publish-docker
  only:
  - master
  - tags
  services:
  - name: docker:dind
    alias: dind
  image: docker:latest
  needs:
  - docker-artifacts-publish
  dependencies:
  - docker-artifacts-publish
  before_script:
  - cd docker-app
  script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - docker pull $CI_REGISTRY_IMAGE:latest || true
  - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
  - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  - docker push $CI_REGISTRY_IMAGE:latest

docker-publish-release-tag:
  stage: publish-docker-release
  only:
  - tags
  services:
  - name: docker:dind
    alias: dind
  image: docker:latest
  needs:
  - docker-publish
  before_script:
  - cd docker-app
  script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
  - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  - docker push $CI_REGISTRY_IMAGE:latest

windows-publish:
  stage: publish-artifacts
  needs:
  - client-build
  - server-build
  dependencies:
  - client-build
  - server-build
  script:
  - dotnet publish DeploymentTrackerCore -c Release -r win-x64 -o deployment-tracker/
  - cp deployment.db deployment-tracker/
  - mv deployment-tracker/DeploymentTrackerCore.exe deployment-tracker/deployment-tracker.exe
  artifacts:
    name: deployment-tracker-windows-x64
    paths:
    - deployment-tracker/
    expire_in: 1 week

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml
