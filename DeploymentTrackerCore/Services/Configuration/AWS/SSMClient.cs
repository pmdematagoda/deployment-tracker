/*
 * This file is part of Deployment Tracker.
 * 
 * Deployment Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Deployment Tracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Deployment Tracker. If not, see <https://www.gnu.org/licenses/>.
 */

#nullable enable

using System;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DeploymentTrackerCore.Services.Configuration.AWS
{
    class SSMClient
    {
        public SSMClient(IConfiguration configuration, IAmazonSimpleSystemsManagement awsSsmClient, ILogger<SSMClient> logger)
        {
            Configuration = configuration;
            AwsSsmClient = awsSsmClient;
            Logger = logger;
        }

        private IConfiguration Configuration { get; }
        public IAmazonSimpleSystemsManagement AwsSsmClient { get; }
        private ILogger<SSMClient> Logger { get; }

        public async Task<SecretsConfiguration> GetSecretsConfiguration() {
            return await GetSecretsConfigurationFromStore();
        }

        private async Task<SecretsConfiguration> GetSecretsConfigurationFromStore() {
            Logger.LogInformation("Fetching secrets from SSM");

            var secretsStoreName = Configuration.GetRequiredSection("AWS_SSM_Stores")["Secrets"];

            if (secretsStoreName == null) {
                Logger.LogError("Secrets store name at [AWS_SSM_Stores].[Secrets] is not configured, and is required");
                throw new InvalidOperationException("Store is not configured!");
            }

            var storeValue = await AwsSsmClient.GetParameterAsync(new GetParameterRequest {
                Name = secretsStoreName,
                WithDecryption = true
            });

            if (storeValue == null) {
                throw new InvalidOperationException("The store was not configured correctly");
            }

            var secretsConfiguration = JsonSerializer.Deserialize<SecretsConfiguration>(storeValue.Parameter.Value);

            if (secretsConfiguration == null) {
                throw new InvalidOperationException("Invalid secrets configuration specified");
            }

            return secretsConfiguration;
        }
    }
}