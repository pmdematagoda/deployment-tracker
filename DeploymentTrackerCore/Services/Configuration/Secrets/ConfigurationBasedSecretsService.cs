/*
 * This file is part of Deployment Tracker.
 * 
 * Deployment Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Deployment Tracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Deployment Tracker. If not, see <https://www.gnu.org/licenses/>.
 */

#nullable enable

using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DeploymentTrackerCore.Services.Configuration.Secrets
{
    class ConfigurationBasedSecretsService : ISecretsConfigurationService
    {
        public ConfigurationBasedSecretsService(IConfiguration configuration, ILogger<ConfigurationBasedSecretsService> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        private IConfiguration Configuration { get; }
        private ILogger<ConfigurationBasedSecretsService> Logger { get; }

        public string GetExternalApiToken()
        {
            var token = Configuration["ExternalToken"];
            
            if (token == null) {
                throw new InvalidOperationException("Token for external API calls is not specified");
            }

            return token;
        }

        public UsernameAndPassword GetJiraCredentials()
        {
            var jiraConfiguration = Configuration.GetRequiredSection("Jira");

            var enabled = Boolean.Parse(jiraConfiguration["Enabled"] ?? Boolean.FalseString);

            if (!enabled) {
                return new UsernameAndPassword("", "");
            }

            var userConfiguration = jiraConfiguration.GetRequiredSection("User");

            var username = userConfiguration["UserName"];
            var password = userConfiguration["Password"];

            if (!String.IsNullOrWhiteSpace(username) && !String.IsNullOrWhiteSpace(password)) {
                Logger.LogInformation($"Using user '{username}' for Jira integration");

                return new UsernameAndPassword(username, password);
            }

            throw new InvalidOperationException("Credentials have not been configured correctly");
        }

        public UsernameAndPassword GetLdapCredentials()
        {
            var ldapSection = Configuration.GetRequiredSection("IdentitySource").GetRequiredSection("Ldapv2");

            var bindUsername = ldapSection["BindUsername"];
            var bindPassword = ldapSection["BindPassword"];

            if (bindUsername == null || bindPassword == null) {
                throw new InvalidOperationException("Bind user details were not specified, if they are meant to be empty then set them to empty strings");
            }

            return new UsernameAndPassword(bindUsername, bindPassword);
        }
    }
}