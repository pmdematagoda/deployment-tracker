/*
 * This file is part of Deployment Tracker.
 * 
 * Deployment Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Deployment Tracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Deployment Tracker. If not, see <https://www.gnu.org/licenses/>.
 */

#nullable enable

using DeploymentTrackerCore.Services.Configuration.AWS;
using Microsoft.Extensions.Configuration;

namespace DeploymentTrackerCore.Services.Configuration.Secrets
{
    class AWSSSMBasedSecretsService : ISecretsConfigurationService
    {
        public AWSSSMBasedSecretsService(IConfiguration configuration, SSMClient ssmClient)
        {
            Configuration = configuration;
            SsmClient = ssmClient;
        }

        private IConfiguration Configuration { get; }
        private SSMClient SsmClient { get; }

        public string GetExternalApiToken()
        {
            var secretsConfiguration = SsmClient.GetSecretsConfiguration().Result;
            
            return secretsConfiguration.ExternalApiToken;
        }

        public UsernameAndPassword GetJiraCredentials()
        {
            var secretsConfiguration = SsmClient.GetSecretsConfiguration().Result;

            return secretsConfiguration.Jira;
        }

        public UsernameAndPassword GetLdapCredentials()
        {
            var secretsConfiguration = SsmClient.GetSecretsConfiguration().Result;

            return secretsConfiguration.LDAP;
        }
    }
}