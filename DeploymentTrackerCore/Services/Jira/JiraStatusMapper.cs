/*
 * This file is part of Deployment Tracker.
 * 
 * Deployment Tracker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Deployment Tracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Deployment Tracker. If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;

namespace DeploymentTrackerCore.Services.Jira {
    public class JiraStatusMapper {
        private IDictionary<string, IDictionary<int, JiraStatus>> ProjectStatusMapping { get; }

        public JiraStatusMapper(IDictionary<string, IDictionary<JiraStatus, IList<int>>> statusMapping) {
            ProjectStatusMapping = InvertMapping(statusMapping);
        }

        public JiraStatus Map(string projectKey, int jiraStatusId) {
            if (!ProjectStatusMapping.ContainsKey(projectKey)) {
                return JiraStatus.UNKNOWN;
            }

            return MapForProject(jiraStatusId, ProjectStatusMapping[projectKey]);
        }

        private static JiraStatus MapForProject(int jiraStatusId, IDictionary<int, JiraStatus> statusMapping) {
            if (!statusMapping.ContainsKey(jiraStatusId)) {
                return JiraStatus.UNKNOWN;
            }

            return statusMapping[jiraStatusId];
        }

        private static Dictionary<string, IDictionary<int, JiraStatus>> InvertMapping(IDictionary<string, IDictionary<JiraStatus, IList<int>>> projectStatusMapping) {
            var invertedMapping = new Dictionary<string, IDictionary<int, JiraStatus>>();

            foreach (var entry in projectStatusMapping) {
                var invertedMappingForProject = new Dictionary<int, JiraStatus>();

                var inProgressIds = entry.Value[JiraStatus.IN_PROGRESS];

                foreach (var id in inProgressIds) {
                    invertedMappingForProject[id] = JiraStatus.IN_PROGRESS;
                }

                var completedIds = entry.Value[JiraStatus.COMPLETED];

                foreach (var id in completedIds) {
                    invertedMappingForProject[id] = JiraStatus.COMPLETED;
                }

                invertedMapping.Add(entry.Key, invertedMappingForProject);
            }

            return invertedMapping;
        }
    }

}