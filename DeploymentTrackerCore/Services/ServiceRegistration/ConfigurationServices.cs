/*
* This file is part of Deployment Tracker.
* 
* Deployment Tracker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Deployment Tracker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Deployment Tracker. If not, see <https://www.gnu.org/licenses/>.
 */

using Amazon.SimpleSystemsManagement;
using DeploymentTrackerCore.Services.Configuration.AWS;
using DeploymentTrackerCore.Services.Configuration.Secrets;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DeploymentTrackerCore.Services.ServiceRegistration
{
    public static class ConfigurationServices
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            var secretsSource = configuration["SecretsSource"];

            if (secretsSource == "AWS_SSM") {
                services.AddSingleton<ISecretsConfigurationService, AWSSSMBasedSecretsService>();
                services.AddSingleton<IAmazonSimpleSystemsManagement, AmazonSimpleSystemsManagementClient>();
                services.AddSingleton<SSMClient>();
            } else {
                services.AddSingleton<ISecretsConfigurationService, ConfigurationBasedSecretsService>();
            }
        }
    }
}