const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const packageJson = JSON.parse(fs.readFileSync('./package.json', 'utf8'));

const esbuild = require('esbuild');
const { ESBuildMinifyPlugin } = require('esbuild-loader');
const { ProvidePlugin } = require('webpack');

module.exports = {
    mode: 'development',
    entry: {
        index: './src/index.js',
        'account.login': './src/login.tsx',
        'account.logout': './src/logout.tsx',
    },
    devtool: 'eval-source-map',
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        path: path.resolve(__dirname, '..', 'wwwroot', 'js'),
        filename: '[name].bundle.js',
    },
    target: 'web',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                resolve: {
                    fullySpecified: false,
                },
            },
            {
                test: /\.m?js$/,
                loader: 'esbuild-loader',
                options: {
                    loader: 'jsx',
                    target: 'es2015',
                    implementation: esbuild,
                },
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader', {
                    loader: 'esbuild-loader',
                    options: {
                        loader: 'css',
                        implementation: esbuild,
                        minify: true,
                    },
                }],
            },
            {
                test: /\.tsx?$/,
                loader: 'esbuild-loader',
                options: {
                    loader: 'tsx',
                    target: 'es2015',
                    implementation: esbuild,
                },
            },
        ],
    },
    optimization: {
        minimizer: [
            new ESBuildMinifyPlugin({
                target: 'es2015',
            }),
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.VERSION': JSON.stringify(packageJson.version),
        }),
        new ProvidePlugin({
            React: 'react',
        }),
    ],
};
