import { Route, Routes, Outlet } from 'react-router-dom';

import EnvironmentList from '../environment/connected-environment-list';
import DeploymentList from '../deployment/connected-deployment-list';
import ApplicationRoute from './application-route';

const Content = () => (
    <>
    <Routes>
        <Route path={ApplicationRoute.Environments} element={<EnvironmentList />} />
        <Route path={ApplicationRoute.Deployments} element={<DeploymentList />} />
    </Routes>
    <Outlet />
    </>
);

export default Content;
