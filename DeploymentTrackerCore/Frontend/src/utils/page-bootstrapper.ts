import { createRoot } from 'react-dom/client';

const boostrapToPage = (component: React.ReactElement): void => {
    const applicationStart = () => {
        const mountPointElement = document.querySelector('.app-mount-point');

        if (!mountPointElement) {
            throw new Error('Unable to start application due to missing mount point');
        }

        const root = createRoot(mountPointElement);

        root.render(component);
    };

    document.addEventListener('DOMContentLoaded', applicationStart, {
        once: true,
    });
};

export default boostrapToPage;
