import 'antd/dist/antd.css';

import bootstrapToPage from './utils/page-bootstrapper';
import LogoutScreen from './account/logout-screen';

bootstrapToPage(<LogoutScreen />);
